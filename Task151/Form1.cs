﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Task151
{
    public partial class Form1 : Form
    {
        SqlDataAdapter adapter1;
        SqlConnection con1 ;
        DataTable dt;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dt = new DataTable();
            con1 = new SqlConnection(Infrastructure.ConnectionStr);
            con1.Open();
            adapter1 = new SqlDataAdapter(@"SELECT Id, Analog.article1 AS ""Артикул1"", Analog.manufacturer1 AS ""Производитель1"", Analog.article2 AS ""Артикул2"", Analog.manufacturer2 AS ""Производитель2"", Analog.trusting AS ""Доверие"" FROM Analog ORDER BY id", con1);
            adapter1.Fill(dt);
            dataGridView_analogi.DataSource = dt;
            dataGridView_analogi.Columns["Id"].Visible = false;
            con1.Close();
        }

        private void DatabaseUpdate()
        {
            con1.Open();
            SqlCommandBuilder combldr = new SqlCommandBuilder(adapter1);
            try
            {
                adapter1.Update(dt);
                MessageBox.Show("Успешно обновлена база данных.", "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e1)
            {
                MessageBox.Show("Не удалось обновить базу данных" + Environment.NewLine + e1.Message.ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally { con1.Close(); }
        }

        private void DataGridView_analogi_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Введите число!");
        }

        private void Button_find_Click(object sender, EventArgs e)
        {
            Find_chains find_Chains = new Find_chains();
            find_Chains.ShowDialog();
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            DatabaseUpdate();
            Form1_Load(this, e);
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            Form1_Load(this, e);
        }

    }
}
