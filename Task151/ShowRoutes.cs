﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Task151
{
    public partial class ShowRoutes : Form
    {
        Routes r;

        public ShowRoutes(Routes r)
        {
            this.r = r;
            InitializeComponent();
        }

        private void ShowRoutes_Load(object sender, EventArgs e)
        {
            listBoxRoutes.Items.Clear();
            for (int i=0;i<r.Route_list.Count;i++)
            {
                listBoxRoutes.Items.Add("Маршрут "+ (i+1));
            }
        }

        private void ListBoxRoutes_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBoxChains.Items.Clear();
            if (listBoxRoutes.SelectedIndex != -1)
                foreach (Chain c in r.Route_list[listBoxRoutes.SelectedIndex].Chains)
                {
                    if (checkBox_NoFormat.Checked)
                        listBoxChains.Items.Add("(" + c.Product_source1.GetArticleNoFormat() + "," + c.Product_source1.GetManufacturerNoFormat()+ ")->(" + c.Product_target1.GetArticleNoFormat() + "," + c.Product_target1.GetManufacturerNoFormat() + ")");
                    else
                        listBoxChains.Items.Add("(" + c.Product_source1.Article + "," + c.Product_source1.Manufacturer + ")->(" + c.Product_target1.Article + "," + c.Product_target1.Manufacturer + ")");
                }
        }

        private void CheckBox_NoFormat_CheckedChanged(object sender, EventArgs e)
        {
            ListBoxRoutes_SelectedIndexChanged(sender,e);
        }
    }
}
