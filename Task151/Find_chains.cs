﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Task151
{
    public partial class Find_chains : Form
    {
        Product target_product;
        SqlConnection con1;
        bool DeleteLoop;
        SqlDataAdapter adapter1;
        DataTable dt;

        public Find_chains()
        {
            InitializeComponent();
        }
        //ищет маршруты от current_product до target_product, если не найдены возвращает null
        private Routes FindRoute(Product current_product, int iteration_left)
        {
            DataRow[] dr= dt.Select(string.Format("[article1_format]='{0}' AND [manufacturer1_format]='{1}'", current_product.Article, current_product.Manufacturer));
            if (dr.Length!= 0) // если нашли аналоги
            {
                Routes r1 = null;//переменная для хранения всех маршрутов на данной итерации
                foreach (DataRow row in dr)
                {
                    //если нашли искомый продукт
                    if (target_product.HasEquals(new Product { Article = (string)row["article2"], Manufacturer = (string)row["manufacturer2"] }))
                    {//добавляем маршрут
                        r1 = Routes.SumRoutes(r1, new Routes() { Route_list = new List<Route> { new Route { Chains = new List<Chain> { new Chain { Product_source1 = current_product, Product_target1 = target_product} } } } });
                    }
                    if ((int)row["trusting"] != 0)
                        if (iteration_left > 1)//если итерации ещё остались запускаем дальнейший поиск, результат поиска добавляем к маршрутам
                            r1 = Routes.SumRoutes(r1, Routes.AddChain(FindRoute(new Product { Article = (string)row["article2"], Manufacturer = (string)row["manufacturer2"] }, iteration_left - 1),
                                new Chain { Product_source1 = current_product, Product_target1 = new Product { Article = (string)row["article2"], Manufacturer = (string)row["manufacturer2"] } }, DeleteLoop));
                }
                return r1;
            }
            else return null;
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            DeleteLoop = checkBox_DeleteLoop.Checked;
            con1 = new SqlConnection(Infrastructure.ConnectionStr);
            con1.Open();
            adapter1 = new SqlDataAdapter(@"SELECT [article2],[manufacturer2],[trusting],[article1],[manufacturer1] FROM Analog", con1);
            dt = new DataTable();
            adapter1.Fill(dt);
            //добавляем к данным ещё столбцы с отформатированными названиями
            DataColumn c1 = new DataColumn("article1_format", typeof(string)); DataColumn c2 = new DataColumn("manufacturer1_format", typeof(string));
            dt.Columns.AddRange(new []{ c1, c2 });
            foreach (DataRow dr in dt.Rows)
            {
                dr["article1_format"] = RegexConvert.ToAlphaNumericOnly((string)dr["article1"]);
                dr["manufacturer1_format"] = (dr["manufacturer1"] as string).ToUpper();
            }
            con1.Close();
            target_product = new Product { Article =textBox_ArticleTarget.Text, Manufacturer =textBox_ManufacturerTarget.Text};
            Routes r = FindRoute(new Product { Article = textBox_ArticleSource.Text, Manufacturer = textBox_ManufacturerSource.Text},Convert.ToInt32(textBox_DepthSearch.Text));
            if (r == null) MessageBox.Show(string.Format( "Искомый товар ({0}) не найден за {1} шаг(а)(ов)",target_product.GetArticleNoFormat()+","+target_product.GetManufacturerNoFormat(),textBox_DepthSearch.Text));
            else
            {
                r.Reverse();
                ShowRoutes showRoutes = new ShowRoutes(r);
                showRoutes.ShowDialog();
            }
        }

        private void Find_chains_Load(object sender, EventArgs e)
        {

        }

        private void TextBox_DepthSearch_Validating(object sender, CancelEventArgs e)
        {
            try { Convert.ToInt32(textBox_DepthSearch.Text); } catch
            {
                MessageBox.Show("Введите число!");
                textBox_DepthSearch.Text = "";
                e.Cancel=true;
            }
        }
    }
}
