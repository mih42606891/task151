﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task151
{
    class Product
    {
        string article;
        string manufacturer;

        public string Article { get { return RegexConvert.ToAlphaNumericOnly(article); } set => article = value; }
        public string Manufacturer { get { return manufacturer.ToUpper(); } set => manufacturer = value; }

        public string GetArticleNoFormat() { return article; }
        public string GetManufacturerNoFormat() { return manufacturer; }
        public bool HasEquals(Product p)
        {
            if ((p.Article== Article) & (p.Manufacturer == Manufacturer)) return true; else return false;
        }
    }
}
