﻿namespace Task151
{
    partial class ShowRoutes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxRoutes = new System.Windows.Forms.ListBox();
            this.listBoxChains = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBox_NoFormat = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // listBoxRoutes
            // 
            this.listBoxRoutes.FormattingEnabled = true;
            this.listBoxRoutes.ItemHeight = 16;
            this.listBoxRoutes.Location = new System.Drawing.Point(12, 50);
            this.listBoxRoutes.Name = "listBoxRoutes";
            this.listBoxRoutes.Size = new System.Drawing.Size(206, 388);
            this.listBoxRoutes.TabIndex = 0;
            this.listBoxRoutes.SelectedIndexChanged += new System.EventHandler(this.ListBoxRoutes_SelectedIndexChanged);
            // 
            // listBoxChains
            // 
            this.listBoxChains.FormattingEnabled = true;
            this.listBoxChains.ItemHeight = 16;
            this.listBoxChains.Location = new System.Drawing.Point(239, 50);
            this.listBoxChains.Name = "listBoxChains";
            this.listBoxChains.Size = new System.Drawing.Size(532, 388);
            this.listBoxChains.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Маршруты";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label2.Location = new System.Drawing.Point(235, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(206, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Содержимое маршрута";
            // 
            // checkBox_NoFormat
            // 
            this.checkBox_NoFormat.AutoSize = true;
            this.checkBox_NoFormat.Checked = true;
            this.checkBox_NoFormat.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_NoFormat.Location = new System.Drawing.Point(501, 18);
            this.checkBox_NoFormat.Name = "checkBox_NoFormat";
            this.checkBox_NoFormat.Size = new System.Drawing.Size(164, 21);
            this.checkBox_NoFormat.TabIndex = 4;
            this.checkBox_NoFormat.Text = "Без фоматирования";
            this.checkBox_NoFormat.UseVisualStyleBackColor = true;
            this.checkBox_NoFormat.CheckedChanged += new System.EventHandler(this.CheckBox_NoFormat_CheckedChanged);
            // 
            // ShowRoutes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 455);
            this.Controls.Add(this.checkBox_NoFormat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxChains);
            this.Controls.Add(this.listBoxRoutes);
            this.Name = "ShowRoutes";
            this.Text = "ShowRoutes";
            this.Load += new System.EventHandler(this.ShowRoutes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxRoutes;
        private System.Windows.Forms.ListBox listBoxChains;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBox_NoFormat;
    }
}