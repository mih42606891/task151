﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task151
{
    public class Routes
    {
        List<Route> route_list;

        internal List<Route> Route_list { get => route_list; set => route_list = value; }

        public static Routes SumRoutes(Routes r1, Routes r2)
        {
            if (r2 != null)
                if (r1 != null)
                {
                    r1.route_list.AddRange(r2.route_list);
                    return r1;
                }
                else return r2;
            else
                return r1;
            
        }
        public static Routes AddChain(Routes r, Chain c,bool DeleteLoop)
        {
            if (r != null)
            {
                int j = 0;
                while (j<r.route_list.Count) 
                {
                    Route r1 = r.route_list[j];
                    if (DeleteLoop)
                    {
                        int i = 0;
                        while ((i < r1.Chains.Count) && (!r1.Chains[i].Product_source1.HasEquals(c.Product_source1))) i++;
                        if (i < r1.Chains.Count)
                            r.route_list.Remove(r1);
                        else
                        {
                            j++;
                            r1.Chains.Add(c);
                        }
                    }
                    else
                    {
                        j++;
                        r1.Chains.Add(c);
                    }
                }
            }
            return r;
        }
        public void Reverse()
        {
            foreach (Route r in Route_list)
            {
                r.Chains.Reverse();
            }
        }
    }
}
