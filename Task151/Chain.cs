﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task151
{
    public class Chain
    {
        Product Product_source;
        Product Product_target;

        internal Product Product_source1 { get => Product_source; set => Product_source = value; }
        internal Product Product_target1 { get => Product_target; set => Product_target = value; }

        public bool HasEquals(Chain c)
        {
            if ((c.Product_source.HasEquals(this.Product_source))&(c.Product_target.HasEquals (this.Product_target))) return true; else return false;
        }
    }
}
