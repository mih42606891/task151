﻿namespace Task151
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_analogi = new System.Windows.Forms.DataGridView();
            this.button_find = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_analogi)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_analogi
            // 
            this.dataGridView_analogi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_analogi.Location = new System.Drawing.Point(12, 4);
            this.dataGridView_analogi.Name = "dataGridView_analogi";
            this.dataGridView_analogi.RowTemplate.Height = 24;
            this.dataGridView_analogi.Size = new System.Drawing.Size(783, 482);
            this.dataGridView_analogi.TabIndex = 0;
            this.dataGridView_analogi.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataGridView_analogi_DataError);
            // 
            // button_find
            // 
            this.button_find.Location = new System.Drawing.Point(438, 513);
            this.button_find.Name = "button_find";
            this.button_find.Size = new System.Drawing.Size(172, 28);
            this.button_find.TabIndex = 1;
            this.button_find.Text = "Найти связь";
            this.button_find.UseVisualStyleBackColor = true;
            this.button_find.Click += new System.EventHandler(this.Button_find_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(48, 513);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(172, 28);
            this.buttonSave.TabIndex = 2;
            this.buttonSave.Text = "Сохранить изменения";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(242, 513);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(172, 28);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Отменить изменения";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 565);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.button_find);
            this.Controls.Add(this.dataGridView_analogi);
            this.Name = "Form1";
            this.Text = "Поиск аналогов";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_analogi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_analogi;
        private System.Windows.Forms.Button button_find;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
    }
}

