﻿namespace Task151
{
    partial class Find_chains
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_ArticleSource = new System.Windows.Forms.TextBox();
            this.textBox_ManufacturerSource = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_ManufacturerTarget = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_ArticleTarget = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_DepthSearch = new System.Windows.Forms.TextBox();
            this.checkBox_DeleteLoop = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(428, 155);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "ПОИСК";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Артикул";
            // 
            // textBox_ArticleSource
            // 
            this.textBox_ArticleSource.Location = new System.Drawing.Point(141, 38);
            this.textBox_ArticleSource.Name = "textBox_ArticleSource";
            this.textBox_ArticleSource.Size = new System.Drawing.Size(146, 22);
            this.textBox_ArticleSource.TabIndex = 2;
            this.textBox_ArticleSource.Text = "KL-9";
            // 
            // textBox_ManufacturerSource
            // 
            this.textBox_ManufacturerSource.Location = new System.Drawing.Point(141, 78);
            this.textBox_ManufacturerSource.Name = "textBox_ManufacturerSource";
            this.textBox_ManufacturerSource.Size = new System.Drawing.Size(146, 22);
            this.textBox_ManufacturerSource.TabIndex = 4;
            this.textBox_ManufacturerSource.Text = "KNecht";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Производитель";
            // 
            // textBox_ManufacturerTarget
            // 
            this.textBox_ManufacturerTarget.Location = new System.Drawing.Point(147, 77);
            this.textBox_ManufacturerTarget.Name = "textBox_ManufacturerTarget";
            this.textBox_ManufacturerTarget.Size = new System.Drawing.Size(146, 22);
            this.textBox_ManufacturerTarget.TabIndex = 8;
            this.textBox_ManufacturerTarget.Text = "FebI";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Производитель";
            // 
            // textBox_ArticleTarget
            // 
            this.textBox_ArticleTarget.Location = new System.Drawing.Point(147, 37);
            this.textBox_ArticleTarget.Name = "textBox_ArticleTarget";
            this.textBox_ArticleTarget.Size = new System.Drawing.Size(146, 22);
            this.textBox_ArticleTarget.TabIndex = 6;
            this.textBox_ArticleTarget.Text = "240/73";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(31, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Артикул";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox_ArticleSource);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox_ManufacturerSource);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(310, 125);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Исходный товар";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.textBox_ArticleTarget);
            this.groupBox2.Controls.Add(this.textBox_ManufacturerTarget);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(362, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(315, 125);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Искомый товар";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(128, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Глубина рекурсии";
            // 
            // textBox_DepthSearch
            // 
            this.textBox_DepthSearch.Location = new System.Drawing.Point(171, 158);
            this.textBox_DepthSearch.Name = "textBox_DepthSearch";
            this.textBox_DepthSearch.Size = new System.Drawing.Size(41, 22);
            this.textBox_DepthSearch.TabIndex = 6;
            this.textBox_DepthSearch.Text = "5";
            this.textBox_DepthSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox_DepthSearch.Validating += new System.ComponentModel.CancelEventHandler(this.TextBox_DepthSearch_Validating);
            // 
            // checkBox_DeleteLoop
            // 
            this.checkBox_DeleteLoop.AutoSize = true;
            this.checkBox_DeleteLoop.Checked = true;
            this.checkBox_DeleteLoop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_DeleteLoop.Location = new System.Drawing.Point(253, 160);
            this.checkBox_DeleteLoop.Name = "checkBox_DeleteLoop";
            this.checkBox_DeleteLoop.Size = new System.Drawing.Size(128, 21);
            this.checkBox_DeleteLoop.TabIndex = 11;
            this.checkBox_DeleteLoop.Text = "Удалять петли";
            this.checkBox_DeleteLoop.UseVisualStyleBackColor = true;
            // 
            // Find_chains
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 235);
            this.Controls.Add(this.checkBox_DeleteLoop);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.textBox_DepthSearch);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Name = "Find_chains";
            this.Text = "Find_chains";
            this.Load += new System.EventHandler(this.Find_chains_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_ArticleSource;
        private System.Windows.Forms.TextBox textBox_ManufacturerSource;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_ManufacturerTarget;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_ArticleTarget;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_DepthSearch;
        private System.Windows.Forms.CheckBox checkBox_DeleteLoop;
    }
}